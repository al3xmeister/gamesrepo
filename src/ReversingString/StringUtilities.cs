﻿namespace CodingChallenge.ReversingString;

public class StringUtilities
{
    public static string Reverse(string s)
    {
        if (!string.IsNullOrWhiteSpace(s))
        {
            char[] stringTextArray = s.ToCharArray();
            Array.Reverse(stringTextArray);
            var reversedString = new String(stringTextArray);
            return reversedString;
        }
        else
        {
            return null;
        };
    }
}
